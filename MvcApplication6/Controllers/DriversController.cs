﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using test1.Models;

namespace MvcApplication6.Controllers
{
    public class DriversController : Controller
    {
        private shipContext db = new shipContext();

        //
        // GET: /Drivers/

        public ActionResult Index()
        {
            return View(db.Drivers.ToList());
        }

        //
        // GET: /Drivers/Details/5

        public ActionResult Details(int id = 0)
        {
            Drivers drivers = db.Drivers.Find(id);
            if (drivers == null)
            {
                return HttpNotFound();
            }
            return View(drivers);
        }

        //
        // GET: /Drivers/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Drivers/Create

        [HttpPost]
        public ActionResult Create(Drivers drivers)
        {
            if (ModelState.IsValid)
            {
                db.Drivers.Add(drivers);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(drivers);
        }

        //
        // GET: /Drivers/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Drivers drivers = db.Drivers.Find(id);
            if (drivers == null)
            {
                return HttpNotFound();
            }
            return View(drivers);
        }

        //
        // POST: /Drivers/Edit/5

        [HttpPost]
        public ActionResult Edit(Drivers drivers)
        {
            if (ModelState.IsValid)
            {
                db.Entry(drivers).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(drivers);
        }

        //
        // GET: /Drivers/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Drivers drivers = db.Drivers.Find(id);
            if (drivers == null)
            {
                return HttpNotFound();
            }
            return View(drivers);
        }

        //
        // POST: /Drivers/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Drivers drivers = db.Drivers.Find(id);
            db.Drivers.Remove(drivers);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}