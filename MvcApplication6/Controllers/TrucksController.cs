﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using test1.Models;

namespace MvcApplication6.Controllers
{
    public class TrucksController : Controller
    {
        private shipContext db = new shipContext();

        //
        // GET: /Trucks/

        public ActionResult Index()
        {
            return View(db.Trucks.ToList());
        }

        //
        // GET: /Trucks/Details/5

        public ActionResult Details(int id = 0)
        {
            Trucks trucks = db.Trucks.Find(id);
            if (trucks == null)
            {
                return HttpNotFound();
            }
            return View(trucks);
        }

        //
        // GET: /Trucks/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Trucks/Create

        [HttpPost]
        public ActionResult Create(Trucks trucks)
        {
            if (ModelState.IsValid)
            {
                db.Trucks.Add(trucks);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(trucks);
        }

        //
        // GET: /Trucks/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Trucks trucks = db.Trucks.Find(id);
            if (trucks == null)
            {
                return HttpNotFound();
            }
            return View(trucks);
        }

        //
        // POST: /Trucks/Edit/5

        [HttpPost]
        public ActionResult Edit(Trucks trucks)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trucks).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(trucks);
        }

        //
        // GET: /Trucks/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Trucks trucks = db.Trucks.Find(id);
            if (trucks == null)
            {
                return HttpNotFound();
            }
            return View(trucks);
        }

        //
        // POST: /Trucks/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Trucks trucks = db.Trucks.Find(id);
            db.Trucks.Remove(trucks);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}