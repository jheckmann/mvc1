﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace test1.Models
{
    public class Cargo
    {
        [Key]
        public int cargID { get; set; }
        public int locationID { get; set; }
        public string cargoDescription { get; set; }
        public string cargoType { get; set; }
    }
}