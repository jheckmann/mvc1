﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace test1.Models
{
    public class Drivers
    {
        [Key]
        public int driverID { get; set; }
        public int truckID { get; set; }
        public int cargoID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}