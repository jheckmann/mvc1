﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace test1.Models
{
    public class Locations
    {
        [Key]
        public int locationID { get; set; }
        public int cargoID { get; set; }
        public string address { get; set; }
    }
}